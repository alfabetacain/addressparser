/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author christian
 */
public class AddressParser
{

    private final String[] roads;
    private final HashMap<String, String> zipToCity;
    private final HashMap<String, String> cityToZip;

    public AddressParser()
    {
        this.cityToZip = new HashMap<>();
        this.zipToCity = new HashMap<>();
        System.out.println(System.getProperty("user.dir"));
        roads = readFile(System.getProperty("user.dir") + "/src/main/road_names.txt").split("[\\r\\n]+");
        loadZipAndCity();
    }

    /**
     * Parses a single string into an address array in the following format:
     * Road name, building number, building letter, floor, zip, city. If any
     * information is missing, that index will be null The address information
     * in the input string must be in the same order
     *
     * @param address the address to parse
     * @return a String array containing the address
     * @throws main.InvalidAddressException
     */
    public String[] parseAddress(String address) throws InvalidAddressException
    {
        String[] returnAddress = new String[6];

        //Check if string contains illegal characters
        Pattern illegalCharacters = Pattern.compile("[?@#¤%&\\\\\\[\\]!\"\'*^~¨;:=`|´{}()<>½§”]");
        Matcher matcherIllegal = illegalCharacters.matcher(address);
        if (matcherIllegal.find())
        {
            address = address.replaceAll("[?@#¤%&\\\\\\[\\]!\"\'*^~¨;:=`|´{}()<>½§]", "");
            System.out.println("############" + address);
            if (address.equals(""))
            {
                throw new InvalidAddressException("String only contains "
                        + "invalid characters");
            }
        }
        else
        {
            //Trim away any space at start or end
            address = address.trim();
            System.out.println("");
            System.out.println("Address = " + address);

            //Find road name if any
            String[] roadname = findRoadInString(address);
            if (roadname != null)
            {
                returnAddress[0] = roadname[1];
                address = address.replaceFirst(roadname[0], "");

                //Find road number if any
                Pattern pattern = Pattern.compile("^([^\\wæøåÆØÅ]{0,1}[\\d]{1,3}[^0-9\\.]){1,1}", Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(address);
                if (matcher.find())
                {
                    String replaceString = matcher.group();
                    replaceString = replaceString.replaceAll("[\\D]", "");
                    returnAddress[1] = cleanString(replaceString);
                    address = address.replaceFirst(replaceString, "");
                    address = cleanBeginningOfString(address);
                }
                //Find building letter if any
                pattern = Pattern.compile("([A-ZÆØÅ]{1,1}[^\\wæøåÆØÅ]{1,1}){1,1}");
                matcher = pattern.matcher(address);
                if (matcher.find())
                {
                    String replaceString = matcher.group();
                    returnAddress[2] = cleanString(replaceString);
                    address = address.replaceFirst(replaceString, "");
                    address = cleanBeginningOfString(address);
                }

                //Find floor if any
                pattern = Pattern.compile("(\\d\\.){1,1}(\\ssal)?", Pattern.CASE_INSENSITIVE);
                matcher = pattern.matcher(address);
                if (matcher.find())
                {
                    String replaceString = matcher.group();
                    returnAddress[3] = cleanString(replaceString.replaceAll("\\D", ""));
                    address = address.replaceFirst(replaceString, "");
                    address = cleanBeginningOfString(address);
                }
            }

            //Find zip if any
            Pattern pattern = Pattern.compile("([0-9]{4,4}){1,1}");
            Matcher matcher = pattern.matcher(address);
            if (matcher.find())
            {
                System.out.println(matcher.group());
                String replaceString = matcher.group();
                returnAddress[4] = findRealZip(cleanString(replaceString));
                address = address.replaceFirst(replaceString, "");
                address = cleanBeginningOfString(address);
            }
            System.out.println("Zip = " + returnAddress[4]);

            //Find city if any
            pattern = Pattern.compile("[a-zA-ZæøåÆØÅ,\\\\.\\\\-]{2,}(\\\\s[a-zA-ZæøåÆØÅ]){0,1}");
            matcher = pattern.matcher(address);
            while (matcher.find() && returnAddress[5] == null)
            {
                returnAddress[5] = findRealCity(cleanString(matcher.group()), returnAddress[4]);
                System.out.println("City is = " + returnAddress[5]);
                if (returnAddress[5] != null && returnAddress[4] != null)
                {
                    String expectedCity = zipToCity.get(returnAddress[4]);
                    if (expectedCity != null && !(returnAddress[5].equals(expectedCity)))
                    {
                        returnAddress[4] = null;
                    }
                }
            }
            System.out.println("Zip is now = " + returnAddress[4]);
        }
        /*String[] names = new String[]
         {
         "road name", "building number", "building letter",
         "floor", "zip code", "city name"
         };

         for (int i = 0; i < returnAddress.length; i++)
         {
         System.out.println(i);
         if (returnAddress[i] == null)
         {
         System.out.println("- No real " + names[i] + " found.");
         }
         }*/

        if (returnAddress[0] == null && returnAddress[4] == null
                && returnAddress[5] == null)
        {
            throw new InvalidAddressException("");
        }
        return returnAddress;
    }

    private String cleanString(String input)
    {
        String output = input.replaceAll("[^a-zA-ZæøåÆØÅ0-9\\s\\.]", "");
        return output.trim();
    }

    private String cleanBeginningOfString(String input)
    {
        String output = input.replaceFirst("^[\\W]*", "");
        output = output.replaceFirst("^(i\\s){0,1}", "");
        return output;
    }

    private String findRealZip(String zip)
    {
        String realZip = null;
        if (zipToCity.containsKey(zip))
        {
            realZip = zip;
        }
        return realZip;
    }

    private String findRealCity(String city, String zip)
    {
        String realCity = null;
        if (cityToZip.containsKey(city))
        {
            realCity = city;
        }
        else
        {
            if (zipToCity.get(zip) != null && zipToCity.get(zip).contains(city))
            {
                realCity = zipToCity.get(zip);
            }
            else
            {
                String[] endings = new String[]
                {
                    "N", "NØ", "Ø", "SØ", "S", "SV", "V",
                    "NV", "K", "C"
                };
                for (int i = 0; i < endings.length && realCity == null; i++)
                {
                    if (cityToZip.containsKey(city + " " + endings[i]))
                    {
                        realCity = city + " " + endings[i];
                    }
                }
            }
        }
        /*
         if (zipToCity.get(zip) != null && zipToCity.get(zip).contains(city)) {
         realCity = city;
         } else if(zipToCity.get(zip) != null && zipToCity.get(zip).contains())
         city = spellCheck(CITY, city);
         if (zip passer med city) {
         realcity = city;
         } else if (zipToCity.get(zip) == null) {
         String[] endings = new String[]{"N", "NØ", "Ø", "SØ", "S", "SV", "V",
         "NV", "K"};
         for (String current : endings) {
         if (cityToZip.containsKey(city + " " + endings));
         {
         realCity = city;
         }
         }
                 
         }*/
        return realCity;
    }

    //Returns a Stringarray, where the first index is the substring to be 
    //replaced, and the second is the real roadname. 
    private String[] findRoadInString(String address)
    {
        Pattern pattern = Pattern.compile("^((\\d){1,2}\\.\\s)?((\\d\\.)?[a-zA-ZæøåÆØÅ\\.éäöÖÄÉ]{1,}((\\s))?){0,}((\\d){4,4}|[^i\\d]{0,1})?");
        Matcher matcher = pattern.matcher(address);
        String[] lostAndFoundAdress = new String[2];
        while (matcher.find())
        {
            String check = matcher.group().trim();
            System.out.println("Matched this: " + check);
            String correctRoad = checkRoadWithSpellCheck(check);
            if (correctRoad != null)
            {
                lostAndFoundAdress[0] = check;
                lostAndFoundAdress[1] = correctRoad;
                return lostAndFoundAdress;
            }
            else
            {
                if (check.contains(" "))
                {
                    for (String word : check.split(" "))
                    {
                        correctRoad = checkRoadWithSpellCheck(word);
                        if (correctRoad != null)
                        {
                            lostAndFoundAdress[0] = word;
                            lostAndFoundAdress[1] = check;
                            return lostAndFoundAdress;
                        }
                    }
                }
            }
        }
        return null;
    }

    private void loadZipAndCity()
    {
        try
        {
            //File path = new File("C:\\test\\zipCity.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("textFiles/zipCity.txt"), "UTF-8"));
            //PrintStream out = new PrintStream(System.out, true, "UTF-8");

            String current;
            while ((current = reader.readLine()) != null)
            {
                String[] ls = current.split("(?<=\\d)(\\s+){1,1}");

                if (ls[0].contains("-"))
                {
                    String[] ranges = ls[0].split("-");
                    int start = Integer.parseInt(ranges[0]);
                    int end = Integer.parseInt(ranges[1]);
                    cityToZip.put(ls[1], "" + start);
                    for (; start <= end; start++)
                    {
                        zipToCity.put("" + start, ls[1]);
                    }
                }
                else
                {
                    zipToCity.put(ls[0], ls[1]);
                    cityToZip.put(ls[1].trim(), ls[0]);
                }
            }
        }
        catch (IOException excep)
        {
            excep.printStackTrace();
        }
    }

    private String readFile(String filename)
    {
        String content = null;

        try
        {
            File file = new File(filename); //for ex foo.txt
            FileReader reader = new FileReader(file);
            char[] chars = new char[(int) file.length()];
            reader.read(chars);
            content = new String(chars);

        }
        catch (IOException e)
        {
        }
        return content;
    }

    public boolean checkRoad(String word)
    {
        return Arrays.binarySearch(roads, word) >= 0;
    }

    public String checkRoadWithSpellCheck(String word)
    {
        //Kan ændres så metoden kan bruges til forskellige DB's.
        //evt. efter et parameter.
        String[] DB = roads;

        //det danske alfabet :)
        char[] abc =
        {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
            'w', 'x', 'y', 'z', 'æ', 'ø', 'å', 'ü', 'é', 'ö'
        };

        ArrayList<String> possibleWords = new ArrayList<>();

        //Hvis tom return null.
        if (word == null || word.length() < 1)
        {
            return null;
        }

        //Tjekker om vejen er i DB.
        if (Arrays.binarySearch(DB, word) >= 0)
        {
            return word;
        }

        // Bytter plads på bogstaverne ved siden af hinanden.
        //for at tjekke evt. tyrkfejl.
        for (int i = 1; i < word.length() - 1; i++)
        {
            possibleWords.add(word.substring(0, i) + word.charAt(i + 1)
                    + word.charAt(i) + word.substring(i + 2));
        }

        // Sletter et bogstav.
        // tjekker om bruger har skrevet et bogstav for meget.
        for (int i = 0; i < word.length(); i++)
        {
            possibleWords.add(word.substring(0, i) + word.substring(i + 1));
        }

        // indsæt et bogstav på alle pladser undtagen første. Hvis de har glemt at skrive et bogstav
        for (int i = 0; i < word.length(); i++)
        {
            for (int j = 0; j < abc.length; j++)
            {
                possibleWords.add(word.substring(0, i) + abc[j] + word.substring(i));
            }
        }

        // erstat et bogstav på alle pladser undtagen første. Hvis de har glemt at skrive et bogstav
        for (int i = 0; i < word.length(); i++)
        {
            for (int j = 0; j < abc.length; j++)
            {
                possibleWords.add(word.substring(0, i) + abc[j] + word.substring(i + 1));
            }
        }
        
        

        //Tjek aa for å og omvendt.
        //returnerer det første af de ord der er i listen.
        for (String p : possibleWords)
        {
            //System.out.println(p);
            if (Arrays.binarySearch(DB, p) >= 0)
            {
                return p;
            }
        }

        return null;
    }

    public static void main(String[] args)
    {
        AddressParser hej = new AddressParser();
        try
        {
            String[] thing = hej.parseAddress("Vestrbrogade 7 1374 København V");
            for (String current : thing)
            {
                System.out.println(current);
            }
        }
        catch (InvalidAddressException excep)
        {
            System.out.println("Exception!");
        }
    }
}
