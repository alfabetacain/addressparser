/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Stinus Thomsen
 * @email smot@itu.dk
 * @version 04-02-2014
 */
public class RoadDB {

    String[] roads;

    public RoadDB(String filePath) {
        roads = readFile(filePath).split("[\\r\\n]+");
    }

    public static void main(String[] args) {
        RoadDB vej;
        vej = new RoadDB("/Users/liv_hovsgaard/Downloads/1aars projekt-2/src/pkg1aars/projekt/road_names.txt");
        System.out.println("1 : " + vej.checkRoadWithSpellCheck("Svanshallsgatan"));
        System.out.println("2 : " + vej.checkRoadWithSpellCheck("Svanshaallsgatan"));
        System.out.println("3 : " + vej.checkRoadWithSpellCheck("Svanshllsgatan"));
        System.out.println("4 : " + vej.checkRoadWithSpellCheck("Svanshlalsgatan"));

        System.out.println("5 : " + vej.checkRoadWithSpellCheck("Svanshollsgatan"));
    }

    private String readFile(String filename) {
        String content = null;
        File file = new File(filename); //for ex foo.txt
        try {
            FileReader reader = new FileReader(file);
            char[] chars = new char[(int) file.length()];
            reader.read(chars);
            content = new String(chars);

        } catch (IOException e) {
        }
        return content;
    }

    public boolean checkRoad(String word) {
        return Arrays.binarySearch(roads, word) >= 0;
    }

    public String checkRoadWithSpellCheck(String word) {
        //Kan ændres så metoden kan bruges til forskellige DB's.
        //evt. efter et parameter.
        String[] DB = roads;

        //det danske alfabet :)
        char[] abc = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
            'w', 'x', 'y', 'z', 'æ', 'ø', 'å', 'ü', 'é', 'ö'};

        ArrayList<String> possibleWords = new ArrayList<String>();


        //Hvis tom return null.
        if (word == null || word.length() < 1) {
            return null;
        }

        //Tjekker om vejen er i DB.
        //Skal evt. ikke køres, men lige nu tager jeg det hele med PHL
        if (Arrays.binarySearch(DB, word) >= 0) {
            return word;
        }

        // Bytter plads på bogstaverne ved siden af hinanden.
        //for at tjekke evt. tyrkfejl.
        for (int i = 1; i < word.length() - 1; i++) {
            possibleWords.add(word.substring(0, i) + word.charAt(i + 1)
                    + word.charAt(i) + word.substring(i + 2));
        }

        // Sletter et bogstav.
        // tjekker om bruger har skrevet et bogstav for meget.
        for (int i = 0; i < word.length(); i++) {
            possibleWords.add(word.substring(0, i) + word.substring(i + 1));
        }



        // indsæt et bogstav på alle pladser undtagen første. Hvis de har glemt at skrive et bogstav
        for (int i = 0; i < word.length(); i++) {
            for (int j = 0; j < abc.length; j++) {
                possibleWords.add(word.substring(0, i) + abc[j] + word.substring(i));
            }
        }

        // erstat et bogstav på alle pladser undtagen første. Hvis de har glemt at skrive et bogstav
        for (int i = 0; i < word.length(); i++) {
            for (int j = 0; j < abc.length; j++) {
                possibleWords.add(word.substring(0, i) + abc[j] + word.substring(i + 1));
            }
        }

        //Tjek aa for å og omvendt.


        //returnerer det første af de ord der er i listen.
        for (String p : possibleWords) {
            //System.out.println(p);
            if (Arrays.binarySearch(DB, p) >= 0) {
                return p;
            }
        }

        return null;
    }
}
