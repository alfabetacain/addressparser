package main;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 *
 * @author Kenneth Ry Ulrik <kulr@itu.dk>
 */
public class AddressParserTest
{

    private static AddressParser ap;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    public AddressParserTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
        ap = new AddressParser();
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    /**
     * Tests the functionality of the 'parseAddress' method in the AddressParser
     * class.
     */
    /*    @Ignore
     @Test //(expected = InvalidAddressException)
     public void testParseAddress()
     {
     try {
     String[][] expectedOutputs = new String[8][6];
     //Expected results for the various tests of the method
     String[] expected_1 = new String[] {"Rued Langgaards Vej", null, null, null, null, null};
     String[] expected_2 = new String[] {"Rued Langgaards Vej", "7", null, "5", null, "København"};
     String[] expected_3 = new String[] {"Rued Langgaards Vej", "7", null, null, "2300", "København"};
     String[] expected_4 = new String[] {"Rued Langgaards Vej", "7", null, "5", null, null};
     String[] expected_5 = new String[] {"Rued Langgaards Vej", null, null, null, null, "København"};
     String[] expected_6 = new String[] {"Rued Langgaards Vej", "7", "A", null, null, "København"};
     //Advanced cases:
     String[] expected_a1 = new String[] {"Rued Langgaards Vej", "6", "B", "5", "2300", "København"};
     String[] expected_a2 = new String[] {"Rued Langgaards Vej", "78", null, null, "2300", "København"};

     //Assigning expected outputs to the double array of expected outputs
     expectedOutputs[0] = expected_1;
     expectedOutputs[1] = expected_2;
     expectedOutputs[2] = expected_3;
     expectedOutputs[3] = expected_4;
     expectedOutputs[4] = expected_5;
     expectedOutputs[5] = expected_6;
     expectedOutputs[6] = expected_a1;
     expectedOutputs[7] = expected_a2;

     String[] inputs = new String[8];
     //Inputs for the various tests of the method
     inputs[0] = "Rued Langgaards Vej";
     inputs[1] = "Rued Langgaards Vej 7, 5. sal, København";
     inputs[2] = "Rued Langgaards Vej 7 2300 København";
     inputs[3] = "Rued Langgaards Vej 7, 5.";
     inputs[4] = "Rued Langgaards Vej i København";
     inputs[5] = "Rued Langgaards Vej 7A København ";
     //Advanced cases:
     inputs[6] = "Rued Langaards Vaj 6, 5. B, København 2300";
     inputs[7] = "København 2300 Royd Langgaards Vej 78";
     //inputs[8] = "Are you kidding me?";
     //Black-box-test-cases:


     for(int i = 0; i < expectedOutputs.length; i++) {
     for(int j = 0; j < 6; j++) {
     String[] actual = ap.parseAddress(inputs[i]);
     assertEquals("Attempting to equal the values of the expected "
     + "and actual Strings. Error occured with input: " + inputs[i], expectedOutputs[i][j], actual[j]);
     }
     }

            
     The advanced cases do not pass, since these cases are not supported yet.
             
     } catch (InvalidAddressException e) {
     System.out.println("Exception occured at attempt to test all sorts of stuff...");
     }
     }
     */
    @Test
    public void testParseAddressEmptyString()
    {
        try
        {
            String[] expected = new String[]
            {
                null, null, null, null, null, null
            };
            String input = "";
            String[] actual = ap.parseAddress(input);

            for (int i = 0; i < expected.length; i++)
            {
                assertEquals("Compares expected and actual outputs for: '" + input + "'.", expected[i], actual[i]);
            }
        }
        catch (InvalidAddressException e)
        {
            System.out.println("Exception occured, at ...EmptyString.");
        }
    }

    @Test(expected = NullPointerException.class)
    public void testParseAddressNull()
    {
        try
        {
            String[] expected = new String[]
            {
                null, null, null, null, null, null
            };
            String input = null;
            String[] actual = ap.parseAddress(input);

            for (int i = 0; i < expected.length; i++)
            {
                assertEquals("Compares expected and actual outputs for: '" + input + "'.", expected[i], actual[i]);
            }
        }
        catch (InvalidAddressException e)
        {
            System.out.println("Exception occured at ...Null.");
        }
    }

    @Test
    public void testParseAddressIllegalCharacter()
    {
        try
        {
            String[] expected = new String[]
            {
                "Nørregade", "67", null, null, "1358", "København"
            };
            String input = "Nørregade 67! 1358 Købehavn";
            String[] actual = ap.parseAddress(input);

            for (int i = 0; i < expected.length; i++)
            {
                assertEquals("Compares expected and actual outputs for: '" + input + "'.", expected[i], actual[i]);
            }
        }
        catch (InvalidAddressException e)
        {
            System.out.println("Exception occured at ...IllegalCharacter.");
        }
    }

    @Test(expected = InvalidAddressException.class)
    public void testParseAddressMultipleIllegalCharacters() throws InvalidAddressException
    {
        String[] expected = new String[]
        {
            null, null, null, null, null, null
        };
        String input = "&&%##";
        String[] actual = ap.parseAddress(input);

        for (int i = 0; i < expected.length; i++)
        {
            assertEquals("Compares expected and actual outputs for: '" + input + "'.", expected[i], actual[i]);
        }
    }

    @Test
    public void testParseAddressFullAddress()
    {
        try
        {
            String[] expected = new String[]
            {
                "Nørregade", "67", "A", "5", "1358", "København K"
            };
            String input = "Nørregade 67A, 5. 1358 København";
            String[] actual = ap.parseAddress(input);

            for (int i = 0; i < expected.length; i++)
            {
                assertEquals("Compares expected and actual outputs for: '" + input + "'.", expected[i], actual[i]);
            }
        }
        catch (InvalidAddressException e)
        {
            System.out.println("Exception occured at ...FullAddress.");
        }
    }

    @Test
    public void testParseAddressWithDeficiencies()
    {
        try
        {
            String[] expected = new String[]
            {
                null, null, null, null, "1358", "København K"
            };
            String input = "67 1358 København";
            String[] actual = ap.parseAddress(input);

            for (int i = 0; i < expected.length; i++)
            {
                assertEquals("Compares expected and actual outputs for: '" + input + "'.", expected[i], actual[i]);
            }
        }
        catch (InvalidAddressException e)
        {
            System.out.println("Exception occured at ...WithDeficiencies.");
        }
    }

    @Test
    public void testParseAddressNonExistent()
    {
        try
        {
            String[] expected = new String[]
            {
                null, null, null, null, "5000", "Odense C"
            };
            String input = "Massemordergade 13 5000 Odense C";
            String[] actual = ap.parseAddress(input);

            for (int i = 0; i < expected.length; i++)
            {
                assertEquals("Compares expected and actual outputs for: '" + input + "'.", expected[i], actual[i]);
            }
        }
        catch (InvalidAddressException e)
        {
            System.out.println("Exception occured at ...NonExistent.");
        }
    }

    @Test
    public void testParseAddressStreetNameSpellingMistake()
    {
        try
        {
            String[] expected = new String[]
            {
                "Nørregade", "67", null, null, "1358", "København K"
            };
            String input = "Nørregode 67 1358 København";
            String[] actual = ap.parseAddress(input);

            for (int i = 0; i < expected.length; i++)
            {
                assertEquals("Compares expected and actual outputs for: '" + input + "'.", expected[i], actual[i]);
            }
        }
        catch (InvalidAddressException e)
        {
            System.out.println("Exception occured at ...StreetNameSpellingMistake.");
        }
    }

    @Test
    public void testParseAddressWrongZip()
    {
        try
        {
            String[] expected = new String[]
            {
                "Nørregade", "67", null, null, null, "Viborg"
            };
            String input = "Nørregade 67 1358 Viborg";
            String[] actual = ap.parseAddress(input);

            for (int i = 0; i < expected.length; i++)
            {
                assertEquals("Compares expected and actual outputs for: '" + input + "'.", expected[i], actual[i]);
            }
        }
        catch (InvalidAddressException e)
        {
            System.out.println("Exception occured at ...WrongZip.");
        }
    }

    @Test
    public void testAddressWithYear()
    {
        try
        {
            String[] expected = new String[]
            {
                "Brabrand Haveforening af 1967", null, null, null,
               "4600", "Køge"
            };
            String input = "Brabrand Haveforening af 1967 4600 Køge";
            String[] actual = ap.parseAddress(input);
            for (int i = 0; i < expected.length; i++)
            {
                assertEquals("Compares expected and actual outputs for: '" + input + "'.", expected[i], actual[i]);
            }
        }
        catch (InvalidAddressException e)
        {
            System.out.println("Exception occured at ...WrongZip.");
        }
    }

}
